const path = require('path');

module.exports = {
    entry: './development/js/entry.js',
    output: {
        path: path.resolve(__dirname, "./assets"),
        filename: "bundled.js",
    },
    module: {
        rules: [
            {
                test: /\.s[ac]ss$/i,
                use: [
                    'style-loader',
                    'css-loader',
                    'sass-loader',
                ],
            },
        ],
    },
}
