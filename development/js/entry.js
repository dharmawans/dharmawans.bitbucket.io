import 'boxicons';
import 'slick-carousel/slick/slick.scss';
import * as helper from './helper';
import './jquery-plugin';
import './TweenMax';

const toggleThemeBtn = document.querySelector('#theme-toggle');
const toggleThemeIcon = toggleThemeBtn.querySelector('box-icon');
toggleThemeBtn.addEventListener('click', helper.toggleTheme);

if (localStorage.getItem('toggled') !== '') {
  document.querySelector('body').classList.toggle('dark-theme');
  toggleThemeIcon.setAttribute('color', 'black');
}
else {
  toggleThemeIcon.setAttribute('color', 'white');
}

const skillItemIcons = document.querySelectorAll('.dark-theme .skills__item .slick-arrow box-icon');
Array.from(skillItemIcons).map(skillItemIcon => skillItemIcon.setAttribute('color', 'white'));

const allImages = document.querySelectorAll('img');
Array.from(allImages).map(image => image.setAttribute("draggable", "false"));

document.querySelector("footer time").innerText = new Date().getFullYear();
document.querySelector('#brand #btn-show-sidebar').addEventListener('click', function () {
  const linkBox = document.querySelectorAll('ul li:not(#brand)');
  const nav = document.querySelector('nav');
  nav.classList.add('nav-scrolled');

  linkBox.forEach(function (link) {
    if (link.classList.contains('show-menu')) {
      link.classList.remove('show-menu');
    }
    else {
      link.classList.add('show-menu');
    }
  });
});

const projectItemLink = document.querySelectorAll('.creation-item-detail__link');
projectItemLink.forEach((itemLink, index) => {
  itemLink.addEventListener('click', () => {
    let targetBox = itemLink.dataset.creationBox;
    let targetBoxContent = document.querySelector(`.creation-item-detail__content ${targetBox}`);
    let siblingBoxContent = helper.getAllSibling(targetBoxContent, targetBoxContent.parentElement);
    let btnItemLinkSiblings = helper.getAllSibling(itemLink, itemLink.parentElement);

    targetBoxContent.classList.add('active');
    siblingBoxContent.forEach(eachSiblingBox => {
      if (eachSiblingBox !== targetBoxContent) eachSiblingBox.classList.remove('active');
    });

    btnItemLinkSiblings.forEach(btnItemLink => {
      if (btnItemLink.classList.contains('active')) {
        btnItemLink.classList.remove('active');
      }
    });

    itemLink.classList.add('active');

  });
});

const projectDetailBtn = document.querySelectorAll('.creation-item__btn');
projectDetailBtn.forEach(detailBtn => {
  detailBtn.addEventListener('click', () => {
    detailBtn.nextElementSibling.classList.add('active');
  });
});

const closeModal = document.querySelectorAll('.close-modal');
closeModal.forEach(closeBtn => {
  closeBtn.addEventListener('click', () => {
    closeBtn.closest('.creation-item-detail').classList.remove('active');
  });
});

TweenMax.to("header h2", 2, {maxHeight: "none", delay: 2.5});

window.addEventListener('scroll', function (){
  const heightHeader = document.querySelector('header').offsetHeight;
  const heightSkillSection = document.querySelector('#skills').offsetHeight;
  const heightNav = document.querySelector('nav').offsetHeight;
  const documentOffset = document.documentElement.scrollTop;
  const documentHeight = heightHeader + heightSkillSection + document.querySelector("#creation").offsetHeight +
      document.querySelector("#about").offsetHeight - heightNav;

  if (documentOffset >= documentHeight) {
    document.querySelector("nav").classList.add('on-bottom');
  }
  else {
    document.querySelector("nav").classList.remove('on-bottom');
  }

  if (document.documentElement.scrollTop > 10) {
    document.querySelector("nav").classList.add("nav-scrolled");
  }
  else {
    const navMenu = document.querySelectorAll('nav li:not(#brand)');
    Array.from(navMenu).map(menu => {
      if (menu.classList.contains('show-menu')) {
        document.querySelector('nav').classList.add('nav-scrolled');
      }
      else {
        document.querySelector('nav').classList.remove('nav-scrolled');
      }
    });
  }

  if (document.documentElement.offsetWidth > 768) {
    if (document.documentElement.scrollTop >= heightHeader + heightSkillSection + 230) {
      TweenMax.to("#about li:first-child", 2.5, {
        left: "0px", opacity: 1, xPercent: "100%"
      });
      TweenMax.to("#about li:nth-child(2)", 2.5, {
        right: "0px", opacity: 1, xPercent: "-100%", delay: 0.5
      });
      TweenMax.to("#about li:nth-child(3)", 2.5, {
        left: "0px", opacity: 1, xPercent: "100%", delay: 1
      });
      TweenMax.to("#about li:last-child", 2.5, {
        right: "0px", opacity: 1, xPercent: "-100%", delay: 1.5
      });
    }

    if (document.documentElement.scrollTop >= heightHeader + heightSkillSection - heightNav - 230) {
      TweenMax.staggerTo("#creation figure", 0.8, {opacity: 1}, 0.3);
      TweenLite.to("#creation figure", 0.5, {ease: Power4.easeOut});
    }
  }
});
