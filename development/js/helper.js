export {getAllSibling, toggleTheme};

function getAllSibling(element, parent) {
    const children = [...parent.children];
    return children.filter(child => child !== element)
}

function toggleTheme() {
    const bodyElement = document.querySelector('body');
    const toggleBtnIcon = document.querySelector('#theme-toggle box-icon');

    if (localStorage['toggled'] !== 'dark-theme') {
        bodyElement.classList.toggle('dark-theme', true);
        toggleBtnIcon.setAttribute('color', 'black');
        localStorage['toggled'] = 'dark-theme';
    }
    else {
        bodyElement.classList.toggle('dark-theme', false);
        toggleBtnIcon.setAttribute('color', 'white');
        localStorage['toggled'] = '';
    }
}