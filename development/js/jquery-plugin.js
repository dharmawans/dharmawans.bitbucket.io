import $ from "jquery";
import 'slick-carousel';

$('.skills__item').slick({
    prevArrow: "<a href='' class='slick-arrow slick-arrow-prev'><box-icon name='left-arrow-alt'></box-icon></a>",
    nextArrow: "<a href='' class='slick-arrow slick-arrow-next'><box-icon name='right-arrow-alt'></box-icon></a>"
});

$(".creation-item--slider").slick({
    infinite: false,
    prevArrow: `<a href='javascript:void(0)' class='slick-arrow slick-arrow-prev left-0'>
                    <box-icon name='left-arrow-alt' color='#fff'></box-icon>
                </a>`,
    nextArrow: `<a href='javascript:void(0)' class='slick-arrow slick-arrow-next right-0'>
                    <box-icon name='right-arrow-alt' color='#fff'></box-icon>
                </a>`
});