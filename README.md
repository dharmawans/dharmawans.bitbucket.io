# This is repository for Bariq Dharmawan portofolio
If you wanna clone it, just type 
`git clone https://dharmawans@bitbucket.org/dharmawans/dharmawans.bitbucket.io.git` 
in your terminal

# Contact me
- Instagram: @bariqdharmawans
- WhatsApp: [0877-7619-6047](https://wa.me/6287776196047)
- Portfolio: [https://dharmawans.bitbucket.io/](https://dharmawans.bitbucket.io/)
